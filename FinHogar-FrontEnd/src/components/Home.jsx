import Create from "./Create"
import List from "./List"


function Home(){
    return (
        <div>
            <h1 className="text-4xl mb-4">Home</h1>
            <Create/>
            <List/>
        </div>
    )
}

export default Home