import {useEffect, useState} from "react"
import axios from "axios"
import {Link} from 'react-router-dom'


function List(){

    const URI = "https://finhogar-backend.vercel.app/api/"
    const [listado, setListado] = useState([])
    useEffect(() => {
        getList()
    }, [])

    const getList = async () => {
        const res = await axios.get(URI)
        setListado(res.data.body)
    }

    const deleteList = async (id) => {
        await axios.delete(`${URI}${id}`)
        getList()
    }

    

    return (
        <>
        <h1 className="text-2xl">List</h1>
            <div className="overflow-x-auto">
            <table className="table w-full">
                <thead>
                    <tr>
                        <th className='p-3'>id</th>
                        <th className='p-3'>Nombre</th>
                        <th className='p-3'>Tipo</th>
                        <th className='p-3'>Procedencia</th>
                        <th className='p-3'>Frecuencia</th>
                        <th className='p-3'>Monto</th>
                        <th className='p-3'>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {listado.map((e, index) =>(
                        <tr key={e}>
                            <th className='p-3'>{index + 1}</th>
                            <th className='p-3'>{e.nombre}</th>
                            <th className='p-3'>{e.tipo}</th>
                            <th className='p-3'>{e.procedencia}</th>
                            <th className='p-3'>{e.frecuencia}</th>
                            <th className='p-3'>{e.monto}</th>
                            <th className='p-3'>
                            <Link className="btn btn-outline btn-success">Editar</Link>
                                <button 
                                    className="btn btn-outline btn-error"
                                    onClick={()=>deleteList(e.id)}
                                    >Delete
                                </button>
                            </th>
                </tr> 
                    ))}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default List