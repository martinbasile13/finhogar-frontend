import {useEffect, useState} from "react"
import axios from "axios"

function Create(){
    const URI = "https://finhogar-backend.vercel.app/api"
    const [nombre, setNombre] = useState('')
    const [tipo, setTipo] = useState('')
    const [procedencia, setProcedencia] = useState('')
    const [frecuencia, setFrecuencia] = useState('')
    const [monto, setMonto] = useState(0)

    const create = async (e)=>{
        await axios.post(URI, {nombre: nombre, tipo: tipo, procedencia: procedencia, frecuencia: frecuencia, monto: monto})
    }


    return (
        <div>
            <h1 className="text-2xl">Create</h1>
            <form onSubmit={create}>
                <div>
                    <input value={nombre} onChange={(e)=> setNombre(e.target.value)} type="text" placeholder="Nombre" className="input input-bordered w-full max-w-xs m-3" />
                    <select value={tipo} onChange={(e)=> setTipo(e.target.value)} className="select select-bordered w-full max-w-xs m-3">
                        <option>Tipo</option>
                        <option>Ingreso</option>
                        <option>Egreso</option>
                    </select>
                    <input value={procedencia} onChange={(e)=> setProcedencia(e.target.value)} type="text" placeholder="Procedencia" className="input input-bordered w-full max-w-xs m-3" />
                    <input value={frecuencia} onChange={(e)=> setFrecuencia(e.target.value)} type="text" placeholder="Frecuencia" className="input input-bordered w-full max-w-xs m-3" />
                    <input value={monto} onChange={(e)=> setMonto(e.target.value)} type="number" placeholder="Monto" className="input input-bordered w-full max-w-xs m-3" />
                    <button type="submit" className="btn btn-center m-3">
                        Enviar
                    </button>
            </div>
        </form>
        </div>
    )
}

export default Create