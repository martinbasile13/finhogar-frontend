import {useState, useEffect} from 'react'
import axios from 'axios'
import {useNavigate, useParams} from 'react-router-dom'

function Edit(){

    const URI = "https://finhogar-backend.vercel.app/api"
    const [editnombre, editsetNombre] = useState('')
    const [edittipo, editsetTipo] = useState('')
    const [editprocedencia, editsetProcedencia] = useState('')
    const [editfrecuencia, editsetFrecuencia] = useState('')
    const [editmonto, editsetMonto] = useState(0)
    const navigate = useNavigate()
    const {id} = useParams()
    const update = async (e)=>{
        e.preventDefault()
        await axios.put(URI+id, {nombre: editnombre, tipo: edittipo, procedencia: editprocedencia, frecuencia: editfrecuencia, monto: editmonto})
        navigate('/')
    }

    useEffect(()=>{
        getEditId()
    },[])

    const getEditId = async ()=>{
        const res = await axios.get(URI+id)
        editsetNombre(res.data.nombre)
        editsetTipo(res.data.tipo)
        editsetProcedencia(res.data.procedencia)
        editsetFrecuencia(res.data.frecuencia)
        editsetMonto(res.data.monto)
    }

    return(
        <>
        <div>
            <h1 className="text-2xl">Update</h1>
            <form onSubmit={update}>
                <div>
                    <input value={editnombre} onChange={(e)=> editsetNombre(e.target.value)} type="text" placeholder="Nombre" className="input input-bordered w-full max-w-xs m-3" />
                    <select value={edittipo} onChange={(e)=> editsetTipo(e.target.value)} className="select select-bordered w-full max-w-xs m-3">
                        <option>Tipo</option>
                        <option>Ingreso</option>
                        <option>Egreso</option>
                    </select>
                    <input value={editprocedencia} onChange={(e)=> editsetProcedencia(e.target.value)} type="text" placeholder="Procedencia" className="input input-bordered w-full max-w-xs m-3" />
                    <input value={editfrecuencia} onChange={(e)=> editsetFrecuencia(e.target.value)} type="text" placeholder="Frecuencia" className="input input-bordered w-full max-w-xs m-3" />
                    <input value={editmonto} onChange={(e)=> editsetMonto(e.target.value)} type="number" placeholder="Monto" className="input input-bordered w-full max-w-xs m-3" />
                    <button type="submit" className="btn btn-center m-3">
                        Actualizar
                    </button>
                </div>
            </form>
        </div>
        </>
    )
}

export default Edit