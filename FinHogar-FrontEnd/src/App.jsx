import React from "react"
import { BrowserRouter, Routes, Route, NavLink} from "react-router-dom"

import Home from "./components/Home"
import Create from "./components/Create"
import List from "./components/List"
import Edit from "./components/Edit"


function App() {


  return (
    <div data-theme="dark">
      <BrowserRouter>
      <div className="drawer">
          <input id="my-drawer-3" type="checkbox" className="drawer-toggle" /> 
          <div className="drawer-content flex flex-col">
    
            <div className="w-full navbar bg-base-300">
              <div className="flex-none lg:hidden">
                  <label htmlFor="my-drawer-3" className="btn btn-square btn-ghost">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-6 h-6 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                  </label>
              </div> 
                <div className="flex-1 px-2 mx-2 text-3xl">FinHogar</div>
                <div className="flex-none hidden lg:block">
                  <ul className="menu menu-horizontal p-0">
                    <li><NavLink to='/'>Home</NavLink></li>
                    <li><NavLink to='/Create'>Create</NavLink></li>
                    <li><NavLink to='/List'>List</NavLink></li>
                  </ul>
                </div>
            </div>
            <div>
              <div className=" m-10 p-10 shadow-2xl shadow-indigo-500 rounded-lg">
                <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/create" element={<Create/>}/>
                  <Route path="/list" element={<List/>}/>
                  <Route path="/edit/:id" element={<Edit/>}/>
                </Routes>
              </div>
              <footer className="footer footer-center p-10 bg-base-200 text-base-content rounded">
                <div className="grid grid-flow-col gap-4">
                  <li><NavLink to='/'>Home</NavLink></li>
                  <li><NavLink to='/Create'>Create</NavLink></li>
                  <li><NavLink to='/List'>List</NavLink></li>
                </div> 
                <div>
                  <p>Copyright © 2022 - All right reserved by Martin Basile Gonzalez</p>
                </div>
              </footer>
        </div>
          </div> 
          <div className="drawer-side">
            <label htmlFor="my-drawer-3" className="drawer-overlay"></label> 
            <ul className="menu p-4 w-80 bg-base-100">
      
              <li><a>Sidebar Item 1</a></li>
              <li><a>Sidebar Item 2</a></li>
      
            </ul>
    
          </div>
      </div>
      </BrowserRouter>
    </div>
  ) 
}

export default App
